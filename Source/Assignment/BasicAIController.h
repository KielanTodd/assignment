// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BasicAIController.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API ABasicAIController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	 virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;


private:
	UPROPERTY()
		TArray<AActor*> Waypoints; // Gets an array of all the waypoints in scene

	
	UFUNCTION()
		AActor* ChooseWaypoint(); // Function to choose the waypoint that the ai should move to
};
