// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GenericTeamAgentInterface.h"
#include "DronePawn.generated.h"


// Forward delcaration
class USpringArmComponent;
class UCameraComponent;
class UCustomMovementComponent;
class ADroneProjectile;
class UAssignmentGameInstance;
class AAssignmentGameModeBase;
class USceneCaptureComponent2D;

UCLASS()
class ASSIGNMENT_API ADronePawn : public APawn, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADronePawn();

	UPROPERTY(EditAnywhere)
		UCustomMovementComponent* DroneMovement; // Gets the custom movement component

	UPROPERTY(EditAnywhere)
		USceneComponent* ProjectileSpawnPoint; // Gets the scene component to create a spawn point for projectile

	UPROPERTY(EditAnywhere)
		TSubclassOf<ADroneProjectile> DroneProjectileClass; // Gets a subclass of the drone projecitle class

	UFUNCTION(BlueprintPure)
		float GetHealth(); // Used for getting the health in blueprints (Showing healthbar)

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* DroneMesh; // Used for setting the mesh in blueprints

	UPROPERTY(EditAnywhere)
		USpringArmComponent* SpringArm; // Used for creating a spring arm component

	UPROPERTY(EditAnywhere)
		USpringArmComponent* MapArm; // Used for creating a spring arm component

	UPROPERTY(EditAnywhere)
		UCameraComponent* Camera; // Used for creating a camera component

	UPROPERTY(EditAnywhere)
		float MaxHealth = 50.0f; // Sets the health of player

	UPROPERTY(EditAnywhere)
		float Health; // Stores the health of player

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override; // Used for taking damage

	UPROPERTY()
		UAssignmentGameInstance* GameInstanceRef; // Gets game instance reference

	UPROPERTY()
		AAssignmentGameModeBase* GameModeRef; // Gets game mode reference

	UPROPERTY(EditAnywhere)
		USceneCaptureComponent2D* MapCamera; // Gets the capture component
};



	
