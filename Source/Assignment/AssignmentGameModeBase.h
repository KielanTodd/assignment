// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AssignmentGameModeBase.generated.h"

class UUserWidget;
class UAssignmentGameInstance;

UCLASS()
class ASSIGNMENT_API AAssignmentGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public: 
	UFUNCTION(BlueprintCallable, Category = "My User Interface")
		void ChangeWidget(TSubclassOf<UUserWidget> WidgetClass); // Creates the function for changing widgets

	UFUNCTION()
		void PointScored(); // Used for scoring points

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "My User Interface")
		TSubclassOf<UUserWidget> StartWidgetClass; // Used for loading the default widget (menu widget)

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "User Interface")
		TSubclassOf<UUserWidget> GameHUDClass; 

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "User Interface")
		TSubclassOf<UUserWidget> GameOverClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "User Interface")
		TSubclassOf<UUserWidget> LevelCompleteClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "User Interface")
		TSubclassOf<UUserWidget> GameHUDWithHealthClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "User Interface")
		TSubclassOf<UUserWidget> LevelSelectClass;

	UPROPERTY()
		UUserWidget* CurrentWidget; // Used for getting the current widget

public:

	UFUNCTION()
		void StartGame(); // Used for controlling the start of the game

	UFUNCTION()
		void GameOver(); // Used for game over

	UFUNCTION(BlueprintPure)
		int GetScore(); // Used for Getting the score for widgets

	UFUNCTION(BlueprintCallable)
		void ChangeLevel(); // Used for changing levels

	UFUNCTION(BlueprintPure)
		FString GetGameOver(); // Used for setting game over text

	UFUNCTION(BlueprintPure)
		int GetTimer();  // Used to get the timer on to text

	UFUNCTION()
		void SetControlText(FString controlText); // Sets the controlled text
	
	UFUNCTION(BlueprintPure)
		FString GetControlText() { return ControlText; } // Gets the controlled text

	UFUNCTION()
		void LevelComplete(); // Used to call the level complete code

	UPROPERTY(EditAnywhere)
		int Score = 0; // Used for setting the score

	UPROPERTY()
		bool IsLevelFiveTriggered = false; // Checks if the level has started 

	UPROPERTY()
		bool IsLevelComplete = false; // Checks if current level is complete

private:
	UFUNCTION()
		void Timer(); // Called when the timer is over

	UFUNCTION()
		void EnableGameInput(); // Used to enable input and hides cursor

	UFUNCTION()
		void DisableGameInput(); // Used to disable input and shows cursor

private:
	UPROPERTY(EditAnywhere)
		int ScoreValue = 1; // Used for incrementing the score

	UPROPERTY(EditAnywhere)
		UAssignmentGameInstance* GameInstanceRef; // Gets a reference to the game instance class
	
	UPROPERTY(EditAnywhere)
		FTimerHandle GameTimer; // Used to create a timer

	UPROPERTY(EditAnywhere)
		float GameDuration = 60.0f; // Stores the defualt game duration

	UPROPERTY(EditAnywhere)
		FString ControlText = ""; // Stores the control text

	UPROPERTY()
		APawn* PlayerPawn; // Used to get access to the player pawn

	UPROPERTY()
		APlayerController* PlayerController; // Used to get access to the player controller

	UPROPERTY(EditAnywhere)
		int PlayerIndex = 0; // Sets the player index
};
