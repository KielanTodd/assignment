// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomPlayerController.h"
#include "DronePawn.h"
#include "DroneProjectile.h"
#include "CustomMovementComponent.h"
#include "Kismet/GameplayStatics.h"


void ACustomPlayerController::BeginPlay()
{
	Super::BeginPlay();
	MyDrone = Cast<ADronePawn>(GetPawn()); // Gets the drone


	FString Map = GetWorld()->GetMapName(); // Gets the name of the current level
	Map.RemoveFromStart(GetWorld()->StreamingLevelsPrefix); // Remove editor prefix
	
	if (Map == "MainMenu" || Map == "LevelSelect") // Checks if it's the main menu
	{
		SetInputMode(FInputModeUIOnly()); // Allows mouse to interact with the ui and does not get captured by the game
		bShowMouseCursor = true; // Shows the cursor
	}
	else if (Map == "GameOver")
	{
		SetInputMode(FInputModeUIOnly()); // Allows mouse to interact with the ui and does not get captured by the game
		bShowMouseCursor = true; // Shows the cursor
	}
	else
	{
		SetInputMode(FInputModeGameOnly()); // Allows mouse to get captured by game
		bShowMouseCursor = false; // Hides the cursor
	}
}


void ACustomPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	check(InputComponent); // Checks the input component 
	InputComponent->BindAxis("MoveForward", this, &ACustomPlayerController::CallForwards); // Sets the button mapping to a function
	InputComponent->BindAxis("MoveUp", this, &ACustomPlayerController::CallUp);
	InputComponent->BindAxis("Strafe", this, &ACustomPlayerController::CallStrafe);
	InputComponent->BindAxis("Turn", this, &ACustomPlayerController::CallTurn);
	InputComponent->BindAction("Fire", IE_Pressed, this, &ACustomPlayerController::Fire); 

}

void ACustomPlayerController::CallForwards(float value) // Calls the move forward function
{
	if (MyDrone) // Checks the drone is in the world
	{
		MyDrone->DroneMovement->MoveForward(value); // Moves the drone

	}

}

void ACustomPlayerController::CallUp(float value)
{
	if (MyDrone)
	{
		MyDrone->DroneMovement->MoveUp(value);
	}
}

void ACustomPlayerController::CallStrafe(float value)
{
	if (MyDrone)
	{
		MyDrone->DroneMovement->Strafe(value);

	}
}

void ACustomPlayerController::CallTurn(float value)
{
	if (MyDrone)
	{
		MyDrone->DroneMovement->Turn(value);
	}
}

void ACustomPlayerController::Fire() // Used for firing projectiles
{
	if (MyDrone->DroneProjectileClass) // Checks if the class exists
	{
		FRotator SpawnRotation = MyDrone->ProjectileSpawnPoint->GetComponentRotation(); // Gets the spawn rotation
		// Spawns the projectile
		ADroneProjectile* TempProjectile = GetWorld()->SpawnActor<ADroneProjectile>(MyDrone->DroneProjectileClass, MyDrone->ProjectileSpawnPoint->GetComponentLocation(), SpawnRotation); // Spawns projectile
		TempProjectile->SetOwner(MyDrone); // Sets the owner
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ShotSound, MyDrone->GetActorLocation()); // Plays sound
	}
}




