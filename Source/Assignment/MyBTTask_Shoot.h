// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "MyBTTask_Shoot.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API UMyBTTask_Shoot : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
	UMyBTTask_Shoot();

protected:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& Owner, uint8* NodeMemory) override; // Executes the code when the task is called
};
