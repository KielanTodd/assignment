// Fill out your copyright notice in the Description page of Project Settings.


#include "DroneProjectile.h"
#include "Target.h"
#include "AIBaseCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "kismet/GameplayStatics.h"

// Sets default values
ADroneProjectile::ADroneProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	projectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh")); // Used for creating the mesh 
	SetRootComponent(projectileMesh); // Sets the main root component
	projectileMesh->SetSimulatePhysics(true); // Used to add physics to the projectile
	projectileMesh->SetNotifyRigidBodyCollision(true); // Used for collisions

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement")); // Used for getting the projectile movement
	ProjectileMovement->MaxSpeed = MovementSpeed; // Sets the max speed
	ProjectileMovement->InitialSpeed = MovementSpeed; // Sets the intial speed
	InitialLifeSpan = 10.0f; // Sets the life span

	

}

// Called when the game starts or when spawned
void ADroneProjectile::BeginPlay()
{
	Super::BeginPlay();
	OnActorHit.AddDynamic(this, &ADroneProjectile::OnHit); // Used for Initializing the on hit function
}

// Called every frame
void ADroneProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADroneProjectile::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	AActor* ProjectileOwner = GetOwner();

	if (OtherActor->GetClass()->IsChildOf(ATarget::StaticClass()) ||
		OtherActor->GetClass()->IsChildOf(AAIBaseCharacter::StaticClass())) // Checks if it has collided with a target
	{
		UGameplayStatics::ApplyDamage(
			OtherActor, //actor that will be damaged
			Damage, //the base damage to apply
			ProjectileOwner->GetInstigatorController(), //controller that caused the damage
			this, //Actor that actually caused the damage
			UDamageType::StaticClass() //class that describes the damage that was done
		);
	}
	else if (OtherActor->ActorHasTag("BreakableWall"))
	{
		UE_LOG(LogTemp, Warning, TEXT("Breakable wall"));

		UPrimitiveComponent* RootComp = Cast<UPrimitiveComponent>(OtherActor->GetRootComponent()); // Gets root component of hit actor
		RootComp->AddImpulse(GetActorRotation().Vector() * ImpluseForce * RootComp->GetMass()); // adds impluse
	}

	if (ProjectileOwner == NULL)
	{
		return;
	}
	SelfActor->Destroy(); // Destroys projectile on collision
}

