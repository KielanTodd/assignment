// Fill out your copyright notice in the Description page of Project Settings.


#include "BoolTriggerBox.h"
#include "Kismet/GameplayStatics.h"
#include "AssignmentGameModeBase.h"

ABoolTriggerBox::ABoolTriggerBox()
{
	OnActorBeginOverlap.AddDynamic(this, &ABoolTriggerBox::OnOverlapBegin); // Binds function
}

void ABoolTriggerBox::BeginPlay()
{
	Super::BeginPlay();
	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Gets access to the game mode
}

void ABoolTriggerBox::OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor) 
{
	IsTriggered = true; // Sets is triggered to true on overlap
	GameModeRef->IsLevelFiveTriggered = IsTriggered; // Set is level five triggered to start ai (needs a better way)
}
