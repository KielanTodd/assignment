// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_Shoot.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "AIBaseCharacter.h"

UMyBTTask_Shoot::UMyBTTask_Shoot()
{
	NodeName = TEXT("Shoot"); // Sets the name of the node
}

EBTNodeResult::Type UMyBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& Owner, uint8* NodeMemory)
{
	Super::ExecuteTask(Owner, NodeMemory);

	if (Owner.GetAIOwner() == nullptr) { return EBTNodeResult::Failed; } // Checks if the AI exists

	AAIBaseCharacter* Character = Cast<AAIBaseCharacter>(Owner.GetAIOwner()->GetPawn()); // Casts to the AI pawn
	if (Character == nullptr) { return EBTNodeResult::Failed; } // Checks if the AI exists

	Character->Shoot(); // Calls the shoot function

	return EBTNodeResult::Succeeded;
}
