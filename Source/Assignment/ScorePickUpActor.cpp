// Fill out your copyright notice in the Description page of Project Settings.


#include "ScorePickUpActor.h"
#include "Components/BoxComponent.h"
#include "AssignmentGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AScorePickUpActor::AScorePickUpActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PickUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Flag Mesh")); // Used to create the mesh
	SetRootComponent(PickUpMesh); // Sets root component

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Box")); // Used to create collision box
	CollisionBox->SetupAttachment(PickUpMesh); // Sets up attachment
	CollisionBox->SetBoxExtent(FVector(152.0f, 152.0f, 480.0f)); // Used to set the size of the collision box
	CollisionBox->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f)); // Used to set the position of the box 
	CollisionBox->SetCollisionProfileName("Trigger"); // Sets the collision to trigger

}

// Called when the game starts or when spawned
void AScorePickUpActor::BeginPlay()
{
	Super::BeginPlay();
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AScorePickUpActor::OnOverlapBegin); // Sets the on overlap begin function

	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Sets the game mode reference
}

// Called every frame
void AScorePickUpActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void AScorePickUpActor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APawn* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0); // Gets player
	 
	if (OtherActor == Player) // Checks if lapped actor is the player
	{
		GameModeRef->Score += 1; // Sets the score
		Destroy(); // Destroys the actor
	}
}

