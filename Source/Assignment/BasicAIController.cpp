// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicAIController.h"
#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"

void ABasicAIController::BeginPlay()
{
	Super::BeginPlay();
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints); // Fills array

	MoveToActor(ChooseWaypoint()); // Moves the actor to selected waypoint
}

void ABasicAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);
	MoveToActor(ChooseWaypoint()); // Moves to selected waypoint on move complete

}

AActor* ABasicAIController::ChooseWaypoint()
{
	int index = FMath::RandRange(0, 1); // Gets random waypoint index and moves the ai to it
	return Waypoints[index]; // Returns the index
}

