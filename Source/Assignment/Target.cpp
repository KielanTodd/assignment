// Fill out your copyright notice in the Description page of Project Settings.


#include "Target.h"
#include "DestructibleComponent.h"
#include "AssignmentGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATarget::ATarget()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component")); // Creates a USceneComponent to stop any issues between mesh and destructible mesh

	TargetMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Target Mesh")); // Used for creating the mesh 
	TargetMesh->SetupAttachment(RootComponent); // Sets the root component

	TargetDMMesh = CreateDefaultSubobject<UDestructibleComponent>(TEXT("Target DM")); // Creates the mesh
	TargetDMMesh->SetupAttachment(RootComponent);  // Sets the root component
	TargetDMMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision); // Turns of the collisions
	TargetDMMesh->SetHiddenInGame(true); // Hides the mesh
	TargetDMMesh->SetSimulatePhysics(true); // Set simulatie physics
}

// Called when the game starts or when spawned
void ATarget::BeginPlay()
{
	Super::BeginPlay();
	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Gets access to the game mode
	
}

// Called every frame
void ATarget::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float ATarget::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Health -= DamageAmount; // Decrements health on each hit
	GameModeRef->PointScored(); // Used for scoring

	if (Health <= MinHealth) // Detects if the target should break
	{
		TargetMesh->SetHiddenInGame(true); // Hides the mesh
		TargetMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision); // Disables it's collisions

		TargetDMMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics); // Enables collisions
		TargetDMMesh->SetHiddenInGame(false); // Shows the destructible mesh
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation(), SoundVolume, SoundPitch); // Plays sound

		TargetDMMesh->AddImpulse(GetActorRotation().Vector() * ImpluseForce * TargetDMMesh->GetMass()); // Adds impluse force when broken
	}


	return DamageAmount;
}

