// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AIBaseCharacter.generated.h"

class AAssignmentGameModeBase;
class UAssignmentGameInstance;

UCLASS()
class ASSIGNMENT_API AAIBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIBaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void Shoot();


private:
	UPROPERTY(EditAnywhere)
		AAssignmentGameModeBase* GameModeRef;

	UPROPERTY(EditAnywhere)
		UAssignmentGameInstance* GameInstanceRef; // Gets game instance reference

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override; // Used for taking damage

	UPROPERTY(EditAnywhere)
		float Damage = 10.0f;

	UPROPERTY(EditAnywhere)
		FVector AIEyesLocation = FVector(20.0f, 0.0f, 0.0f);

	UPROPERTY(EditAnywhere)
		int PlayerIndex = 0;

public:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* AIEyes; // Used to shoot arrays from
};
