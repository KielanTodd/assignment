// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "ControlTriggerBox.generated.h"

class AAssignmentGameModeBase;

UCLASS()
class ASSIGNMENT_API AControlTriggerBox : public ATriggerBox
{
	GENERATED_BODY()

public:
	AControlTriggerBox();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		void OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor); // Used on overlap function

	UFUNCTION()
		void OnOverlapEnd(AActor* OverlappedActor, AActor* OtherActor);  // Used on overlap end function

private:
	UPROPERTY()
		AAssignmentGameModeBase* GameModeRef; // Used to create the reference to game mode

	UPROPERTY(EditAnywhere)
		FString TriggerText; // Used to set the text in editor

	UPROPERTY(EditAnywhere)
		FString ExitText;  // Used to set the text in editor

	UPROPERTY(EditAnywhere)
		bool DoOnce = false; // Used to detect if the trigger has been exited or not
};