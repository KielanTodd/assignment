// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ScorePickUpActor.generated.h"

class UBoxComponent;
class AAssignmentGameModeBase;

UCLASS()
class ASSIGNMENT_API AScorePickUpActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AScorePickUpActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PickUpMesh; // Used to create the mesh

	UPROPERTY(EditAnywhere)
		UBoxComponent* CollisionBox; // Creates a collision box 

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult); // Used to check when collision box is entered

	UPROPERTY(EditAnywhere)
		AAssignmentGameModeBase* GameModeRef; // Used to create the reference to game mode
};
