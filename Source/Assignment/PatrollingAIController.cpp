// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrollingAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AIBaseCharacter.h"
#include "DrawDebugHelpers.h" // Testing only

void APatrollingAIController::BeginPlay()
{
	Super::BeginPlay();

	if (AIBehaviour != nullptr)
	{
		RunBehaviorTree(AIBehaviour); // Starts the behaviour tree
	}
}

void APatrollingAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0); // Gets the player pawn

	if (BetterLineOfSightTo(PlayerPawn, 300.0f)) // Calls the better line of sight function and checks if ture
	{
		GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), PlayerPawn->GetActorLocation()); // Sets player location
	}
}

bool APatrollingAIController::BetterLineOfSightTo(AActor* Other, float Range)
{
	AAIBaseCharacter* OwnerPawn = Cast<AAIBaseCharacter>(GetPawn()); // Gets the AI character pawn

	FVector Location = OwnerPawn->AIEyes->GetComponentLocation(); // Gets the location of the eyes
	FRotator Rotation = OwnerPawn->AIEyes->GetComponentRotation(); // Gets the rotation of the eyes

	FVector End = Location + Rotation.Vector() * Range; // Sets the end point of the cast 
	FHitResult Hit; // Stores the hit result

	// Used to ignore collisions with itself
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(OwnerPawn);


	FCollisionShape CollisionShape = FCollisionShape::MakeBox(FVector(100.0f, 20.0f, 150.0f)); // Creates a collision box 


	bool hitSuccess = GetWorld()->SweepSingleByChannel(Hit, Location, End, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, CollisionShape, Params); // Calls raycast and checks if its succesful

	AActor* HitActor = Hit.GetActor(); // Stores the hit actor
	if (hitSuccess && HitActor == Other) // if raycast as hit correct actor
	{
		return true;

	}

	return false;
}

