// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DronePawn.h"
#include "Components/ActorComponent.h"
#include "CustomMovementComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ASSIGNMENT_API UCustomMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Delcares all the functions required to move the drone
	void MoveForward(float AxisValue);
	void MoveUp(float AxisValue);
	void Strafe(float AxisValue);
	void Turn(float AxisValue);


private:
	UPROPERTY(EditAnywhere)
		float MoveSpeedHorizontally = 500.0f; // Used to set the drones movement speed

	UPROPERTY(EditAnywhere)
		float MoveSpeedVertically = 200.0f; // Used to set the drones movement speed

	UPROPERTY(EditAnywhere)
		float RotationSpeed = 200.0f; // Used to set the drones roation speed

	UPROPERTY(EditAnywhere)
		int MIN = 40.0f; // Used for the minimum value the drone can move down to

	UPROPERTY(EditAnywhere)
		int MAX = 300.0f; // Used for the maximum the drone can move up to
};