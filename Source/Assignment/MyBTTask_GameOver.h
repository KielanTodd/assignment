// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "MyBTTask_GameOver.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API UMyBTTask_GameOver : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	UMyBTTask_GameOver();

protected:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& Owner, uint8* NodeMemory) override; // Calls the execute task function 
};
