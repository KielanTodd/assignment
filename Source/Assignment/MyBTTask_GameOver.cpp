// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_GameOver.h"
#include "AssignmentGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "AssignmentGameInstance.h"

UMyBTTask_GameOver::UMyBTTask_GameOver()
{
	NodeName = TEXT("Game Over"); // Sets the node name
}

EBTNodeResult::Type UMyBTTask_GameOver::ExecuteTask(UBehaviorTreeComponent& Owner, uint8* NodeMemory)
{
	Super::ExecuteTask(Owner, NodeMemory);

	if (Owner.GetAIOwner() == nullptr) { return EBTNodeResult::Failed; } // Checks if the AI exists

	AAssignmentGameModeBase* GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Gets access to the game mode class
	if (GameModeRef == nullptr) { return EBTNodeResult::Failed; }

	UAssignmentGameInstance* GameInstanceRef = Cast<UAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())); // Casts to the game instance
	if (GameInstanceRef == nullptr) { return EBTNodeResult::Failed; }

	GameInstanceRef->SetIsDead(true); // Sets the character to dead
	GameInstanceRef->SetReasonText(TEXT("You were spotted!")); // Sets the reason text
	GameModeRef->GameOver(); // Calls game over

	return EBTNodeResult::Succeeded;
}
