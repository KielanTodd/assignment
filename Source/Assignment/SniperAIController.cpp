// Fill out your copyright notice in the Description page of Project Settings.


#include "SniperAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "BoolTriggerBox.h"
#include "AssignmentGameModeBase.h"

void ASniperAIController::BeginPlay()
{
	Super::BeginPlay();

	if (AIBehaviour != nullptr)
	{
		RunBehaviorTree(AIBehaviour); // Starts behaviour tree
	}

	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Gets access to the game mode
}

void ASniperAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0); // Gets player

	if (GameModeRef->IsLevelFiveTriggered) // Checks if the level 5 has beeen triggered to start AI
	{
		GetBlackboardComponent()->SetValueAsObject(TEXT("Player"), PlayerPawn); // Sets the oject value 
	}
}
	


