// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Target.generated.h"

class UProjectileMovementComponent;
class UDestructibleComponent;
class AAssignmentGameModeBase;

UCLASS()
class ASSIGNMENT_API ATarget : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ATarget();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UPROPERTY(EditAnywhere)
		USceneComponent* ParentObj; // Creates a parent object

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* TargetMesh; // Used for getting the target mesh
	
	UPROPERTY(EditAnywhere)
		UDestructibleComponent* TargetDMMesh; // used for getting a destructible mesh

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override; // Used for taking damage

	UPROPERTY(EditAnywhere)
		float Health = 3.0f; // Used for setting health

	UPROPERTY(EditAnywhere)
		float MinHealth = 0.0f; // Used for checking when target dies

	UPROPERTY(EditAnywhere)
		AAssignmentGameModeBase* GameModeRef; // Used for getting reference of game mode

	UPROPERTY(EditAnywhere)
		USoundBase* HitSound; // Gets a sound

	UPROPERTY(EditAnywhere)
		float SoundVolume = 2.0f; // Sets sound volume

	UPROPERTY(EditAnywhere)
		float SoundPitch = 2.0f; // Sets sound pitch

	UPROPERTY(EditAnywhere)
		float ImpluseForce = 10000.0f; // Sets impulse force value
};
