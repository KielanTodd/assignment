// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ChaseAIController.generated.h"

class AAssignmentGameModeBase;
class UAssignmentGameInstance;

UCLASS()
class ASSIGNMENT_API AChaseAIController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

private:
	UFUNCTION()
		bool BetterLineOfSightTo(AActor* Other, float Range); // Function used as a better version of line of sight

	UFUNCTION()
		bool CollisionBox(AActor* Other); // Creates a collision box function

	UPROPERTY(EditAnywhere)
		UBehaviorTree* AIBehaviour; // Gets reference to a behaviour tree

	UPROPERTY()
		AAssignmentGameModeBase* GameModeRef; // Gets referance to the game mode class

	UPROPERTY()
		UAssignmentGameInstance* GameInstanceRef; // Gets referance to the game instance class

	UPROPERTY(EditAnywhere)
		float LineOfSightRange = 300.0f; // Sets the range for line of sight
};
