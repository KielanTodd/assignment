// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTService_SetRandomWaypoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/TargetPoint.h" 

UMyBTService_SetRandomWaypoint::UMyBTService_SetRandomWaypoint()
{
	NodeName = TEXT("Set Random Waypoint"); // Sets the node name
}

void UMyBTService_SetRandomWaypoint::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints); // Fills array

	OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), ChooseWaypoint()->GetActorLocation()); // Sets the value of random waypoint
}

AActor* UMyBTService_SetRandomWaypoint::ChooseWaypoint()
{
	int index = FMath::RandRange(0, Waypoints.Num() - 1); // Finds the random waypoint 
	return Waypoints[index]; // Returns the random value
}