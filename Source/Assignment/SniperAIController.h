// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SniperAIController.generated.h"

class ABoolTriggerBox;
class AAssignmentGameModeBase;

UCLASS()
class ASSIGNMENT_API ASniperAIController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
		UBehaviorTree* AIBehaviour; // Gets behaviour tree

	UPROPERTY()
		AAssignmentGameModeBase* GameModeRef; // Used to create the reference to game mode
};
