// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "AssignmentGameInstance.generated.h"

UCLASS()
class ASSIGNMENT_API UAssignmentGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION()
		void SetIsDead(bool isDead); // Used to set the dead bool

	UFUNCTION()
		bool GetIsDead() { return IsDead; } // Used to get the dead bool

	UFUNCTION()
		void SetReasonText(FString reasonText); // Used to set the reason text

	UFUNCTION(BlueprintPure)
		FString GetReasonText() { return ReasonText; } // Used to get the reason text

private:

	// Member variables
	UPROPERTY()
		bool IsDead = false; 

	UPROPERTY()
		FString ReasonText = "";
};
