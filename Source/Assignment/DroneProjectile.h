// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DroneProjectile.generated.h"

class UProjectileMovementComponent;

UCLASS()
class ASSIGNMENT_API ADroneProjectile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ADroneProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:
	UPROPERTY(EditAnywhere)
		float MovementSpeed = 900.0f; // Sets the movement speed of the projectile

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* projectileMesh; // Used for getting the projectile mesh

	UPROPERTY(EditAnywhere)
		UProjectileMovementComponent* ProjectileMovement; // Used for getting the projectile movement component

	UPROPERTY(EditAnywhere)
		float Damage = 1.0f; // Used to deal amount of damage

	UFUNCTION()
		void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse,
			const FHitResult& Hit); // Used for getting a function to detect when the projectile has hit something

	UPROPERTY(EditAnywhere)
		float ImpluseForce = 5000.0f; // Sets impluse force
};