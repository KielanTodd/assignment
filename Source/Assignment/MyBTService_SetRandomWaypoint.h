// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "MyBTService_SetRandomWaypoint.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API UMyBTService_SetRandomWaypoint : public UBTService_BlackboardBase
{
	GENERATED_BODY()

public:
	UMyBTService_SetRandomWaypoint(); // Contructor

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override; // Calls the tick node function

private:
	UFUNCTION()
		AActor* ChooseWaypoint(); // Used to choose a waypoint in game at random 

	UPROPERTY()
		TArray<AActor*> Waypoints; // Used to get an array of all the waypoints in the world
};
