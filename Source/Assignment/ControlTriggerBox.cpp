// Fill out your copyright notice in the Description page of Project Settings.


#include "ControlTriggerBox.h"
#include "Kismet/GameplayStatics.h"
#include "AssignmentGameModeBase.h"

AControlTriggerBox::AControlTriggerBox()
{
	OnActorBeginOverlap.AddDynamic(this, &AControlTriggerBox::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AControlTriggerBox::OnOverlapEnd);
}

void AControlTriggerBox::BeginPlay()
{
	Super::BeginPlay();
	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Gets access to the game mode
}

void AControlTriggerBox::OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor)
{
	GameModeRef->SetControlText(TriggerText); // Writes out text to the screen when there is an overlap
}

void AControlTriggerBox::OnOverlapEnd(AActor* OverlappedActor, AActor* OtherActor)
{ 
	if (DoOnce)
	{
		Destroy(); // If Do Once set to true in editor then destroy if not the text will stay every time the box is entered
	}
	GameModeRef->SetControlText(ExitText); // Writes out text to the screen when there is an overlap
}
