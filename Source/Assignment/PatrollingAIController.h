// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "PatrollingAIController.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API APatrollingAIController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

private:
	UFUNCTION()
		bool BetterLineOfSightTo(AActor* Other, float Range); // Function for my better line of sight function

	UPROPERTY(EditAnywhere)
		UBehaviorTree* AIBehaviour; // Gets the behaviour tree

};
