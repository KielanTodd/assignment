// Fill out your copyright notice in the Description page of Project Settings.


#include "AssignmentGameInstance.h"

// A game instance is persistant through the game mode so you can save and set data to be carried through each level

void UAssignmentGameInstance::SetIsDead(bool isDead)
{
    IsDead = isDead; // Sets is dead
}

void UAssignmentGameInstance::SetReasonText(FString reasonText)
{
    ReasonText = reasonText; // Sets the reason Text
}
