// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomMovementComponent.h"
#include "DroneProjectile.h"
#include "AssignmentGameModeBase.h"
#include "kismet/GameplayStatics.h"

void UCustomMovementComponent::MoveForward(float AxisValue) // Used for moving the drone forwards and backwards
{
	FVector DeltaLocation = FVector(AxisValue * MoveSpeedHorizontally * GetWorld()->DeltaTimeSeconds, 0.0f, 0.0f); // Gets the axis value (1.0 or -1.0) the multiplies it by speed than world units on correct axis
	GetOwner()->AddActorLocalOffset(DeltaLocation, true); // Moves the drone forwards or backwards

}

void UCustomMovementComponent::MoveUp(float AxisValue) // Used for moving the drone up and down
{
	if (AxisValue < 0 && GetOwner()->GetActorLocation().Z > MIN) // Used for stopping the drone from going too low down and clipping into the floor
	{
		FVector DeltaLocation = FVector(0.0f, 0.0f, -1.0f * MoveSpeedVertically * GetWorld()->DeltaTimeSeconds);
		GetOwner()->AddActorLocalOffset(DeltaLocation, true); // Moves drone down
	}
	else if (AxisValue > 0 && GetOwner()->GetActorLocation().Z < MAX) // Used for stopping the drone moving too high
	{
		FVector DeltaLocation = FVector(0.0f, 0.0f, 1.0f * MoveSpeedVertically * GetWorld()->DeltaTimeSeconds);
		GetOwner()->AddActorLocalOffset(DeltaLocation, true); // Moves drone up
	}

}

void UCustomMovementComponent::Strafe(float AxisValue) // Used for making the drone moving left and right
{
	FVector DeltaLocation = FVector(0.0f, AxisValue * MoveSpeedHorizontally * GetWorld()->DeltaTimeSeconds, 0.0f);
	GetOwner()->AddActorLocalOffset(DeltaLocation, true); // Moves left or right

}

void UCustomMovementComponent::Turn(float AxisValue) // Used for rotating the drone
{
	float RotationAmount = AxisValue * RotationSpeed * GetWorld()->DeltaTimeSeconds; // Gets the axis value and time it by rotation speed and frames per seconds
	FRotator Rotation = FRotator(0.0f, RotationAmount, 0.0f); // Sets rotation value on the correct axis
	FQuat DeltaRotation = FQuat(Rotation); // Sets the rotation to quaternion
	GetOwner()->AddActorLocalRotation(DeltaRotation, true); // Rotates the drone
}


