// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CustomPlayerController.generated.h"

class ADronePawn;

UCLASS()
class ASSIGNMENT_API ACustomPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override; // Used for initializing when the game starts

public:
	// Declares functions so that the drone can call the custom movement component class
	virtual void SetupInputComponent(); // Used for setting up the inputs 
	virtual void CallForwards(float value); // Used for calling the move forwards function from the custom movement component class
	virtual void CallUp(float value);
	virtual void CallStrafe(float value);
	virtual void CallTurn(float value);
	void Fire(); // Used for fireing the projectile

	ADronePawn* MyDrone; // Gets the class that is being moved

private:
	UPROPERTY(EditAnywhere)
		USoundBase* ShotSound;
};
