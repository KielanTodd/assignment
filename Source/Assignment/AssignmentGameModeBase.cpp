// Copyright Epic Games, Inc. All Rights Reserved.

#include "AssignmentGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "AssignmentGameInstance.h"

void AAssignmentGameModeBase::ChangeWidget(TSubclassOf<UUserWidget> WidgetClass)
{
	if (CurrentWidget != nullptr) // Checks if  widget is null or not
	{
		CurrentWidget->RemoveFromViewport(); // Removes widget from viewport
		CurrentWidget = nullptr; // Sets widget to null
	}
	if (WidgetClass != nullptr) // Checks if widget class is null or not
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetClass); // Creates widget
		if (CurrentWidget != nullptr) 
		{
			CurrentWidget->AddToViewport(); // Adds widget to viewport
		}
	}
}

void AAssignmentGameModeBase::PointScored()
{
	UE_LOG(LogTemp, Warning, TEXT("Scored called"));
	Score += ScoreValue; // Increments score every time the player scores
}

void AAssignmentGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	StartGame(); // Calls the start game function

	GameInstanceRef = Cast<UAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())); // Casts to the game instance

	PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), PlayerIndex); // Gets the player pawn in the world at index 0
	PlayerController = GetWorld()->GetFirstPlayerController();

	FString Map = GetWorld()->GetMapName(); // Gets the map name
	Map.RemoveFromStart(GetWorld()->StreamingLevelsPrefix); // Removes editor prefix
	if (Map == "MainMenu") // If main menu is loaded then open main menu widget
	{
		ChangeWidget(StartWidgetClass);  // Calls the change widget function and swaps the or opens widget
	}
	else if(Map == "GameOver")
	{
		ChangeWidget(GameOverClass);
	}
	else if (Map == "LevelSelect")
	{
		ChangeWidget(LevelSelectClass);
	}
	else
	{
		ChangeWidget(GameHUDClass);
	}

	if (Map == "LevelOne")
	{
		SetControlText("WASD to Move"); // Sets the control text 
	}
	else if (Map == "LevelTwo")
	{
		SetControlText("Score as many points as possible"); // Sets the control text 
	}
	else if (Map == "LevelThree")
	{
		SetControlText("Don't get caught by the guards!"); // Sets the control text 
	}
	else if (Map == "LevelFour")
	{
		SetControlText("Don't get caught by the chaser!"); // Sets the control text 
	}
	else if (Map == "LevelFive")
	{
		ChangeWidget(GameHUDWithHealthClass);
		SetControlText("Don't get killed by the Sniper!"); // Sets the control text 
	}
}


void AAssignmentGameModeBase::StartGame()
{
	UE_LOG(LogTemp, Warning, TEXT("Start Game"));

	FString Map = GetWorld()->GetMapName(); // Gets the map name
	Map.RemoveFromStart(GetWorld()->StreamingLevelsPrefix); // Removes editor prefix

	if (Map != "MainMenu" || Map != "LevelSelect")
	{
		GetWorld()->GetTimerManager().SetTimer(GameTimer, this, &AAssignmentGameModeBase::Timer, GameDuration, false); // Starts the time for all levels but main menu
	}
}

void AAssignmentGameModeBase::GameOver()
{
	UGameplayStatics::OpenLevel(GetWorld(), "GameOver"); // Opens Game Over level when called
}

int AAssignmentGameModeBase::GetScore()
{
	return Score; // Gets the score to be used by widgets
}

void AAssignmentGameModeBase::ChangeLevel()
{
	FString Map = GetWorld()->GetMapName(); // Gets the map name
	Map.RemoveFromStart(GetWorld()->StreamingLevelsPrefix); // Removes editor prefix
	if (Map == "MainMenu")
	{
		UGameplayStatics::OpenLevel(GetWorld(), "LevelOne"); // Opens level when called
	}
	else if (Map == "LevelOne") // Checks the level and then opens correct level
	{ 
		UGameplayStatics::OpenLevel(GetWorld(), "LevelTwo"); // Opens level when called
	}
	else if (Map == "LevelTwo")
	{
		UGameplayStatics::OpenLevel(GetWorld(), "LevelThree"); // Opens level when called
	}
	else if (Map == "LevelThree")
	{
		UGameplayStatics::OpenLevel(GetWorld(), "LevelFour"); // Opens level when called
	}
	else if (Map == "LevelFour")
	{
		UGameplayStatics::OpenLevel(GetWorld(), "LevelFive"); // Opens level when called
	}
	else if (Map == "LevelFive")
	{
		UGameplayStatics::OpenLevel(GetWorld(), "MainMenu"); // Opens level when called
	}
}

FString AAssignmentGameModeBase::GetGameOver()
{
	if (GameInstanceRef->GetIsDead())
	{
		return FString(TEXT("You Lose!")); // Sets the game over text 
	}
	else if (!GameInstanceRef->GetIsDead())
	{
		return FString(TEXT("You Win!"));
	}
	else
	{
		return FString(TEXT("Game Over!"));
	}
}

int AAssignmentGameModeBase::GetTimer()
{
	return GetWorld()->GetTimerManager().GetTimerRemaining(GameTimer); // Gets the timer and outputs it to widget
}

void AAssignmentGameModeBase::SetControlText(FString controlText)
{
	ControlText = controlText; // Sets the control text 
}

void AAssignmentGameModeBase::Timer()
{
	FString Map = GetWorld()->GetMapName(); // Gets the map name
	Map.RemoveFromStart(GetWorld()->StreamingLevelsPrefix); // Removes editor prefix

	if (Map == "LevelOne" || Map == "LevelThree" || Map == "LevelFour" || Map == "LevelFive")
	{ 
		GameInstanceRef->SetIsDead(true); // Sets is dead to true
		GameInstanceRef->SetReasonText(TEXT("You ran out of time!")); // Sets why the game is over
		GameOver(); // Calls the game over
	}
	else if (Map == "LevelTwo")
	{
		LevelComplete(); // Calls the level complete function
	}
}

void AAssignmentGameModeBase::EnableGameInput()
{
	PlayerController->SetInputMode(FInputModeGameOnly()); // Sets the input to game only
	PlayerController->bShowMouseCursor = false; // Hides the cursor
}

void AAssignmentGameModeBase::DisableGameInput()
{
	PlayerController->SetInputMode(FInputModeUIOnly()); // Sets the input to ui only
	PlayerController->bShowMouseCursor = true; // Shows the cursor
}

void AAssignmentGameModeBase::LevelComplete()
{
	if (Score > 0) // If the score is bigger than 0 open level complete
	{
		ChangeWidget(LevelCompleteClass); // Changes user widget
		DisableGameInput(); // Disables game input and shows cursor
		IsLevelComplete = true; // Sets level Complete
	}
	else 
	{
		GameInstanceRef->SetIsDead(true); // Sets the text to you lose
		GameInstanceRef->SetReasonText(TEXT("You Didn't Score Points!")); // Sets the reason text
		GameOver(); // Calls the game over function 
	}
}
