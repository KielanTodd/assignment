// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "FlagPole.generated.h"

class UBoxComponent;
class AAssignmentGameModeBase;
class UAssignmentGameInstance;

UCLASS()
class ASSIGNMENT_API AFlagPole : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AFlagPole();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult); // Used to check when collision box is entered

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FlagMesh; // Used to create the mesh

	UPROPERTY(EditAnywhere)
		UBoxComponent* CollisionBox; // Used to create the collision box

	UPROPERTY(EditAnywhere)
		AAssignmentGameModeBase* GameModeRef; // Used to create the reference to game mode

	UPROPERTY(EditAnywhere)
		UAssignmentGameInstance* GameInstanceRef; // Gets a reference to the game instance
};
