// Fill out your copyright notice in the Description page of Project Settings.


#include "AIBaseCharacter.h"
#include "AssignmentGameModeBase.h"
#include "AssignmentGameInstance.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AAIBaseCharacter::AAIBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AIEyes = CreateDefaultSubobject<USceneComponent>(TEXT("AI Eyes Component")); // Creates a scene object to use as eyes
	AIEyes->SetupAttachment(RootComponent); // Attaches to the root object
	AIEyes->SetRelativeLocation(AIEyesLocation); // Sets the Eye location (However it's better to do in blueprint)
}



// Called when the game starts or when spawned
void AAIBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Gets access to the game mode
	GameInstanceRef = Cast<UAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())); // Casts to the game instance
}

// Called every frame
void AAIBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GameModeRef->IsLevelComplete) // Destroys all AI on level complete
	{
		Destroy();
	}
}

void AAIBaseCharacter::Shoot()
{
	FVector Location; 
	FRotator Rotation;
	GetActorEyesViewPoint(Location, Rotation); // Sets the Location and Rotation

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), PlayerIndex);

	FVector End = PlayerPawn->GetActorLocation(); // Sets the end location

	FHitResult Hit; // Creates a hit result 

	// Collision ignores itself 
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	bool success = GetWorld()->LineTraceSingleByChannel(Hit, Location, End, ECollisionChannel::ECC_GameTraceChannel1, Params); // Checks if there has been a hit 
	
	if (success) // if ray has hit
	{
		AActor* HitActor = Hit.GetActor(); // Gets the actor that was hit
		if (HitActor != nullptr) // Checks if actor exists
		{
			// Apply Damage
			UGameplayStatics::ApplyDamage(
				PlayerPawn, //actor that will be damaged
				Damage, //the base damage to apply
				GetOwner()->GetInstigatorController(), //controller that caused the damage
				this, //Actor that actually caused the damage
				UDamageType::StaticClass() //class that describes the damage that was done
			);
		}
	}
}



float AAIBaseCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	GameModeRef->PointScored();

	return DamageAmount;
}

