// Fill out your copyright notice in the Description page of Project Settings.


#include "DronePawn.h"
#include "CustomMovementComponent.h"
#include "DroneProjectile.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "AssignmentGameInstance.h"
#include "AssignmentGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SceneCaptureComponent2D.h"

// Sets default values
ADronePawn::ADronePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	DroneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Drone Mesh")); // Creates a default mesh for the drone
	SetRootComponent(DroneMesh); // Sets the main root component to the drone mesh

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm")); // Creates a spring arm component to be setup
	SpringArm->SetupAttachment(RootComponent); // Sets the attachment
	SpringArm->SetRelativeRotation(FRotator(-20.f, 0.f, 0.f)); // Sets the pitch of the spring arm
	SpringArm->TargetArmLength = 200.0f; // Sets the length of the spring arm
	SpringArm->bEnableCameraLag = true; // Sets the camera lag
	SpringArm->CameraLagSpeed = 3.0f; // Sets the speed of camera lag speed

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera")); // Sets defualt camera
	Camera->SetupAttachment(SpringArm); // Sets the camera to spring arm

	DroneMovement = CreateDefaultSubobject<UCustomMovementComponent>(TEXT("Drone Movement")); // Sets the drone movement 

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point")); // Creates the drone projectile spawn point
	ProjectileSpawnPoint->SetupAttachment(DroneMesh); // Sets the attachment to the drone
	ProjectileSpawnPoint->SetRelativeLocation(FVector(16.489441f, -0.002197f, -5.017166)); // Sets the location of the spawn point

	MapArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Map Arm")); // Creates a spring arm component to be setup
	MapArm->SetupAttachment(RootComponent); // Sets the attachment
	MapArm->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f)); // Sets the rotation of the spring arm

	MapCamera = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("Map Camera")); // Creates a capture component
	MapCamera->SetupAttachment(MapArm); // Sets the attachment

	Health = MaxHealth; // Sets the health
}

float ADronePawn::GetHealth()
{
	return Health / MaxHealth; // Gets the health and changes it to between 0 and 1
}

void ADronePawn::BeginPlay()
{
	GameInstanceRef = Cast<UAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())); // Casts to the game instance
	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Casts to game reference
}

float ADronePawn::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	Health -= DamageAmount; // removes health

	if (Health <= 0.0f) // If dead
	{
		GameInstanceRef->SetIsDead(true); // Sets is dead to true
		GameInstanceRef->SetReasonText(TEXT("You were killed!")); // Sets why the game is over
		GameModeRef->GameOver(); // Calls the game over
	}
	return Health;
}
