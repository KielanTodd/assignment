// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "BoolTriggerBox.generated.h"

class AAssignmentGameModeBase;

UCLASS()
class ASSIGNMENT_API ABoolTriggerBox : public ATriggerBox
{
	GENERATED_BODY()
	
public:
	ABoolTriggerBox();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		void OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor); // Used on overlap function

	UFUNCTION()
		void SetIsTriggered(bool isTriggered) { IsTriggered = isTriggered; } // Used for setting the trigger text

	UFUNCTION()
		bool GetIsTriggered() { return IsTriggered; } // Gets the trigger text

private:
	UPROPERTY()
		AAssignmentGameModeBase* GameModeRef; // Used to create the reference to game mode

	UPROPERTY()
		bool IsTriggered = false; // Member variable to store is triggered
};
