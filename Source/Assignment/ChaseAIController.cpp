// Fill out your copyright notice in the Description page of Project Settings.


#include "ChaseAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AIBaseCharacter.h"
#include "AssignmentGameModeBase.h"
#include "AssignmentGameInstance.h"

void AChaseAIController::BeginPlay()
{
	Super::BeginPlay();

	if (AIBehaviour != nullptr)
	{
		RunBehaviorTree(AIBehaviour); // Starts the behaviour tree if it exists
	}

	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Gets access to the game mode class
	GameInstanceRef = Cast<UAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())); // Casts to the game instance
}

void AChaseAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0); // Gets player pawn

	if (BetterLineOfSightTo(PlayerPawn, LineOfSightRange)) // Gets the custom line of sight (line of sight to player and checks range)
	{
		GetBlackboardComponent()->SetValueAsObject(TEXT("Player"), PlayerPawn); // Sets blackboard value
	}
	if (CollisionBox(PlayerPawn)) // Checks if there as been a collision
	{
		GameInstanceRef->SetIsDead(true); // Sets the player is dead
		GameInstanceRef->SetReasonText(TEXT("You were caught!")); // Sets the reason text
		GameModeRef->GameOver(); // Calls game over
	}
}

bool AChaseAIController::BetterLineOfSightTo(AActor* Other, float Range)
{
	/*** My better line of sight to adds a range to check for ***/

	AAIBaseCharacter* OwnerPawn = Cast<AAIBaseCharacter>(GetPawn()); // Gets the AI character pawn

	FVector Location = OwnerPawn->AIEyes->GetComponentLocation(); // Gets the location of the eyes
	FRotator Rotation = OwnerPawn->AIEyes->GetComponentRotation(); // Gets the rotation of the eyes

	FVector End = Location + Rotation.Vector() * Range; // Sets the end point of the cast 
	FHitResult Hit; // Stores the hit result

	// Used to ignore collisions with itself
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(OwnerPawn);


	FCollisionShape CollisionShape = FCollisionShape::MakeBox(FVector(100.0f, 20.0f, 150.0f)); // Creates a collion box shape 

	bool hitSuccess = GetWorld()->SweepSingleByChannel(Hit, Location, End, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, CollisionShape, Params); // Creates a ray and checks if it returns true

	AActor* HitActor = Hit.GetActor(); // Stores the hit actor
	if (hitSuccess && HitActor == Other) // Checks if the ray has hit and hit the correct actor
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool AChaseAIController::CollisionBox(AActor* Other)
{
	AAIBaseCharacter* OwnerPawn = Cast<AAIBaseCharacter>(GetPawn()); // Gets the AI character pawn

	FVector Location = OwnerPawn->GetActorLocation(); // Sets the location
	Location.Z += 80.0f; // Offset

	FHitResult Hit; // Stores the hit result

	// Used to ignore collisions with itself
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(OwnerPawn);

	FCollisionShape CollisionShape = FCollisionShape::MakeBox(FVector(50.0f, 50.0f, 150.0f)); // Creates collision box
	
	bool hitSuccess = GetWorld()->SweepSingleByChannel(Hit, Location, Location, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel1, CollisionShape, Params); // Checks if the raycast was successful

	AActor* HitActor = Hit.GetActor(); // Stores the hit actor
	if (hitSuccess && HitActor == Other) // Checks if hit was successful with other actor
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

