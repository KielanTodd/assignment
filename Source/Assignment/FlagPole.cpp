// Fill out your copyright notice in the Description page of Project Settings.


#include "FlagPole.h"
#include "Components/BoxComponent.h"
#include "AssignmentGameModeBase.h"
#include "AssignmentGameInstance.h"

// Sets default values
AFlagPole::AFlagPole()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FlagMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Flag Mesh")); // Used to create the mesh
	SetRootComponent(FlagMesh);

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Box")); // Used to create collision box
	CollisionBox->SetupAttachment(FlagMesh); // Used to set parent object
	CollisionBox->SetBoxExtent(FVector(152.0f, 152.0f, 480.0f)); // Used to set the size of the collision box
	CollisionBox->SetRelativeLocation(FVector(0.0f, 85.0f, 451.0f)); // Used to set the position of the box 
	CollisionBox->SetCollisionProfileName("Trigger"); // Sets the collision to trigger

}

// Called when the game starts or when spawned
void AFlagPole::BeginPlay()
{
	Super::BeginPlay();
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AFlagPole::OnOverlapBegin); // Sets the on overlap begin function

	GameModeRef = Cast<AAssignmentGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())); // Sets the game mode reference
	GameInstanceRef = Cast<UAssignmentGameInstance>(UGameplayStatics::GetGameInstance(GetWorld())); // Sets the game instance reference
}

// Called every frame
void AFlagPole::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFlagPole::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APawn* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0); // Gets the player

	if(OtherActor == Player) // Checks if overlapped actor is the player
	{
		GameModeRef->LevelComplete(); // Calls the level complete function
	}
}


